# E-line Icon Pack

e-line.ua

##Usage
npm i e-line-icons

$e-icons-path: "~e-line-icons/fonts";

@import "~e-line-icons/scss/e-line-icons.scss";

## License
MIT

0.X.X => 1.0.0:
Icons for e-line 2.0 projects
Station types - station-(a,b,c,d,e,f)
socket types: gbt/ccs/...

0.0.X => 0.1.X:

icon-bars-ext -> icon-bars-extended
arrow-down ->  chevron-down
arrow-left -> chevron-left
arrow-right ->  chevron-right
arrow-up -> chevron-up
ccscombo -> ccs-combo
cross-rounded -> close-circle
check-rounded -> check-circle
eline -> e-line
station -> f4

## Changelog
1.0.1 [17 September] style.css
1.0.0 [17 September] new station types, socket types, etc..
0.1.2 [30 Jan ] icon-guest-charge
0.1.1 [17 January 2017]*changelog*
added:
sort-up
sort-down
battery
manually
leaf
stopwatch
facebook
Deleted:
guest-charge (same to f4)
0.1.0 [17 January 2017] - Alpha release
0.0.5 [15 January 2017] - big update, 100 icons!;
0.0.4 [13 January 2017] - Added new icons;
               <-- holidays xD -->
0.0.3 [06 November 2017] - Icons update;
0.0.2 [23 October  2017] - SCSS Init;
0.0.1 [22 October  2017] - Repository Init;
